//
//  LoginViewController.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    let defaults = ManagerUserDefaults()
    
    var viewModelLogin = LoginViewModel()
    
    var parametrosLogin = [String: Any]()
    
    var infoDeLogin = RequestTokenResponse() {
        didSet {
            viewModelLogin.requestSession(parameters: ["request_token" : defaults.getToken(key: "TOKEN")])
        }
    }
    
    var token = ""
    
    var username = ""
    var password = ""
    
    var requestTokenAuth = String() {
        didSet {
            if requestTokenAuth == "" {
                
            } else {
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        enableKeyBoardToggle()
        setupHideKeyboardOnTap()
        setupView()
        
        viewModelLogin.statusCodeRequestToken.bind { [weak self] code in
            //
            self?.hideLoader()
            if code == .success {
            }
        }
        
        viewModelLogin.responseRequestToken.bind { [weak self] response in
            self?.requestTokenAuth = response.requestToken!
            
            self?.defaults.saveToken(token: response.requestToken!)
            
            self?.parametrosLogin = [
                "username": self!.username,
                "password": self!.password,
                "request_token": response.requestToken
            ]
            
            self?.defaults.saveUserName(username: self!.username)
            self?.viewModelLogin.makeLogin(parameters: self?.parametrosLogin)
        }
        
        viewModelLogin.statusCodeLogin.bind { [weak self] statusCode in
            self?.hideLoader()
            if statusCode == .unauthorized {
                self?.errorLabel.text = self?.infoDeLogin.statusMessage
                self?.errorLabel.alpha = 1
                return
            }
            if statusCode != .success {
                return
            }
            if statusCode == .success {
                
            }
        }
        
        viewModelLogin.responseRequestLogin.bind { [weak self] response in
            self?.infoDeLogin = response
            
        }
        
        viewModelLogin.responseSession.bind {[weak self] responseSession in
            if responseSession.success == false {
                
            } else {
                self?.defaults.saveStatusLogged(status: 1)
                self?.defaults.saveSessionId(session: responseSession.session_id!)
                let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
                let rootController = storyboard.instantiateViewController(withIdentifier: "homeNavigationVC") as! UINavigationController
                self?.view.window?.rootViewController = rootController
                self?.view.window?.makeKeyAndVisible()
            }
        }
        
        
    }
    
    func setupView() {
        errorLabel.numberOfLines = 0
        errorLabel.lineBreakMode = .byWordWrapping
        errorLabel.alpha = 0
        
        usernameTextfield.delegate = self
        passwordTextfield.delegate = self
        
        //Change status bar background
        self.navigationController?.setStatusBar(backgroundColor: UIColor(named: "tm_navigation_background")!)
        
        navigationController?.navigationBar.barStyle = .black
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    // MARK: - Keyboard rearrange view
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func keyboardWillChangeLogic(isShow: Bool, keyboardHeight: CGFloat) {
        UIView.animate(withDuration: 0.25) {
            self.view.frame.origin.y = -(isShow ? (keyboardHeight/2) : 0)
            self.view.layoutIfNeeded()
        }
    }
    
    func isAnyErrorInFields() -> Bool {
        let username = usernameTextfield.text!
        let password = passwordTextfield.text!
        var error = false
        var errorText: String = ""
        
        if username.isEmpty {
            errorLabel.text = "Error. Llena todos los es campos."
            errorLabel.alpha = 1
            error = true
        }
        
        if password.isEmpty {
            errorLabel.text = "Error. Llena todos los es campos."
            errorText = "Error. Llena todos los es campos."
            errorLabel.alpha = 1
            error = true
        }
        
        if errorText != "" {
            errorLabel.text = errorText
        }
        
        if error == true {
            errorLabel.alpha = 1
            return true
        } else {
            return false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        clearErrorLabel()
        
        return true
    }
    func clearErrorLabel() {
        errorLabel.alpha = 0
    }
    
    @IBAction func loginButtonClick(_ sender: Any) {
        //viewModelLogin.requestToken()
        
        if isAnyErrorInFields() == true {
            return
        }
        
        
        username = usernameTextfield.text!
        password = passwordTextfield.text!
        
        
        if NetworkMonitor.isConnectedToNetwork() == false{
            //mostrar alerta
            let alert = UIAlertController(title: "Error de red",
                                        message: "Verifica tu conexión a internet",
                                          preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok",
                                          style: UIAlertAction.Style.default,
                                    handler: nil
            ))
            self.present(alert, animated: true, completion: nil)
            hideLoader()
            return
        } else {
            viewModelLogin.requestToken()
            showLoader()
            
        }
    }
}
