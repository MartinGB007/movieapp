//
//  LoginModel.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import Foundation

struct RequestTokenResponse: Codable {
    var success: Bool?
    var expiresAt: String?
    var requestToken: String?
    var statusMessage: String?
    
    private enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "request_token"
        case statusMessage = "status_message"
    }
    
}

struct RequestSessionResponse: Codable {
    var success: Bool?
    var session_id: String?
    
}

struct LoginResponse: Codable {
    //var token: String
    var accessToken: String
    var expiresIn: Int
    var tokenType: String
    var scope: String
    var refreshToken: String
    
    private enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case tokenType = "token_type"
        case scope
        case refreshToken = "refresh_token"
    }
}
