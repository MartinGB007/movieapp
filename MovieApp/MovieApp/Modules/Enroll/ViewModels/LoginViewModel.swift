//
//  LoginViewModel.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import Foundation

public class LoginViewModel {
    var responseRequestToken = Observable(RequestTokenResponse())
    var statusCodeRequestToken = Observable(CodeResponse.success)
    //var CodeRestore = Observable(CodeResponse.success)
    
    var responseSession = Observable(RequestSessionResponse())
    var statusCodeSession = Observable(CodeResponse.success)
    
    
    var responseRequestLogin = Observable(RequestTokenResponse())
    var statusCodeLogin = Observable(CodeResponse.success)
    
    
    init(){
        
    }
    
    func requestToken() {
        Api.requestToken(url: ApiConstants.requestToken, method: .GET) { [weak self] (response: RequestTokenResponse?,  statusCode) in
            guard let response = response else {
                self?.statusCodeRequestToken.value = statusCode
                return
            }
            
            if statusCode == .success {
                self?.responseRequestToken.value = response
                self?.statusCodeRequestToken.value = statusCode
            }
        }
    }
    
    func requestSession(parameters: [String: Any]?) {
        Api.requestSession(url: ApiConstants.requestSession, method: .POST, parameters: parameters) { [weak self] (response: RequestSessionResponse?,  statusCode) in
            guard let response = response else {
                self?.statusCodeRequestToken.value = statusCode
                return
            }
            
            if statusCode == .success {
                self?.statusCodeSession.value = statusCode
                self?.responseSession.value = response
            }
        }
    }
    
    func makeLogin(parameters: [String: Any]?) {
        Api.requestMakeLogin(url: ApiConstants.requestLogin, method: .POST, parameters: parameters) { [weak self](response: RequestTokenResponse?, statusCode) in
            
            guard let response = response else {
                self?.statusCodeLogin.value = statusCode
                return
            }
            
            self?.responseRequestLogin.value = response
            self?.statusCodeLogin.value = statusCode
        }
    }
}
