//
//  HomeViewController.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    let defaults = ManagerUserDefaults()
    
    var movieResponse = MovieResponse() {
        didSet {
            hideLoader()
            DispatchQueue.main.async {
                self.moviesCollectionView.reloadData()
            }
        }
    }
    
    
    var viewModelMovies = MovieViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        moviesCollectionView.register(MovieCollectionViewCell.nib(), forCellWithReuseIdentifier: MovieCollectionViewCell.identifier)
        moviesCollectionView.dataSource = self
        moviesCollectionView.delegate = self
        moviesCollectionView.collectionViewLayout = UICollectionViewFlowLayout()
        
        moviesCollectionView.contentInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        
        setupView()
        
        viewModelMovies.statusCode.bind { [weak self] code in
            self?.hideLoader()
            if code != .success {
                //handle
            }
            if code == .success {
                //handle
            }
        }
        
        viewModelMovies.response.bind { [weak self] movies in
            self?.movieResponse = movies
        }
        
        viewModelMovies.getPopularMovies()
        
    }
    
    func setupView() {
        if #available(iOS 13.0, *) {
            segmentedControl.layer.borderColor = UIColor.white.cgColor

              let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            segmentedControl.setTitleTextAttributes(titleTextAttributes, for:.normal)

            let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
            segmentedControl.setTitleTextAttributes(titleTextAttributes1, for:.selected)
        } else {
            segmentedControl.tintColor = .white
            segmentedControl.backgroundColor = .black
            segmentedControl.layer.cornerRadius = 4
        }
        
        self.title = "TV Shows"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "tm_white")!]
        self.navigationController?.navigationBar.backgroundColor = UIColor(named: "tm_navigation_background")
        
        
        
        //Change status bar background
        self.navigationController?.setStatusBar(backgroundColor: UIColor(named: "tm_navigation_background")!)
        
        navigationController?.navigationBar.barStyle = .black
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    
    @IBAction func menuButtonClick(_ sender: Any) {
        let menu = UIAlertController(title: nil, message: "What do you want to do ?", preferredStyle: UIAlertController.Style.actionSheet)
        
        menu.addAction(UIAlertAction(title: "View Profile", style: .default, handler: { _ in
            
            let story = UIStoryboard(name: "Profile", bundle: nil)
            if let vc = story.instantiateViewController(withIdentifier: "profileVC") as?  ProfileViewController{
                
                self.present(vc, animated: true, completion: nil)
            }
        } ))
        menu.addAction(UIAlertAction(title: "Log out", style: .destructive, handler: { _ in
                Session.closeSession(actualView: self.view)
        } ))
        
        
        menu.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            menu.dismiss(animated: true, completion: nil)
        }))
        self.present(menu, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func switchTabs(_ sender: Any) {
        
        if NetworkMonitor.isConnectedToNetwork() == true {
        
            if segmentedControl.selectedSegmentIndex == 0 {
                showLoader()
                viewModelMovies.getPopularMovies()
            } else if segmentedControl.selectedSegmentIndex == 1 {
                showLoader()
                viewModelMovies.getOnTv()
            } else if segmentedControl.selectedSegmentIndex == 2 {
                showLoader()
                viewModelMovies.getAiring()
            } else if segmentedControl.selectedSegmentIndex == 3 {
                showLoader()
                viewModelMovies.getTopRated()
            }
        } else {
            let alert = UIAlertController(title: "No internet", message: "Check your netwrok connection.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieResponse.getCountOfMovies()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.identifier, for: indexPath) as! MovieCollectionViewCell
        
        cell.movie = movieResponse.movies![indexPath.row]
        
        return cell
    }
}


extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 340)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
