//
//  MovieCollectionViewCell.swift
//  MovieApp
//
//  Created by Martín González on 13/01/22.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var movieImage: CustomImageView!
    
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        // stop your player here
        // set your label text = ""
        movieImage.image = UIImage()
        movieTitleLabel.text = ""
        dateLabel.text = ""
        descriptionLabel.text = ""
        rateLabel.text = ""
    }
    
    var movie: Movie? {
        didSet {
            if movie?.movieImageURL == nil {
                movieImage.image = UIImage(named: "tm_logo")
            } else {
                movieImage.downloaded(from: (movie?.movieImageURL)!)
            }
            
            if movie?.title == nil {
                movieTitleLabel.text = movie?.name ?? "Sin título"
            } else {
                movieTitleLabel.text = movie?.title ?? "Sin titulo"
            }
            
            descriptionLabel.text = movie?.overview
            let rate = movie?.rate! ?? 0
            rateLabel.text = String(rate)
            
            guard let fecha = movie?.year?.convertIntoDateFormated(fromFormat: "yyyy-mm-dd", toFormat: "MMM dd, yyyy") else {
                dateLabel.text = "Release date"
                return
            }
            
            dateLabel.text = fecha
        }
    }
    
    static let identifier = "MovieCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "MovieCollectionViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Utilities.styleBackViewMenu(backView)
        
        descriptionLabel.numberOfLines = 4
        descriptionLabel.lineBreakMode = .byTruncatingTail
        
        imageBackView.clipsToBounds = false
        imageBackView.layer.shadowColor = UIColor.black.cgColor
        imageBackView.layer.shadowOpacity = 0.3
        imageBackView.layer.shadowOffset = CGSize.zero
        imageBackView.layer.shadowRadius = 3
        imageBackView.layer.shadowPath = UIBezierPath(roundedRect: imageBackView.bounds, cornerRadius: 3).cgPath
        
        
        self.movieImage.layer.masksToBounds = true
        self.movieImage.layer.cornerRadius = 8
        
    }

}
