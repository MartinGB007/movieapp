//
//  Movie.swift
//  MovieApp
//
//  Created by Martín González on 13/01/22.
//

import Foundation


struct MovieResponse : Codable {
    let movies : [Movie]?
    
    private enum CodingKeys: String, CodingKey {
        case movies = "results"
    }
    
    init() {
        movies = []
    }
    
    func getCountOfMovies() -> Int{
        guard let movies = movies else {
            return 1
        }

        return movies.count
    }
}

struct Movie : Codable {
    let id: Int
    let title: String?
    let year: String?
    let rate: Double?
    let posterImage: String?
    let overview: String?
    let name: String?
    let firstAirDate: String?
    
    private enum CodingKeys: String, CodingKey {
        case id, title, overview, name
        case year = "release_date"
        case rate = "vote_average"
        case posterImage = "poster_path"
        case firstAirDate = "first_air_date"
    }
    
    var movieImageURL: String? {
        get {
            
            guard let image = posterImage else {return nil}
            return ApiConstants.getMovieImage(fileName: image)
            
        }
    }
}

