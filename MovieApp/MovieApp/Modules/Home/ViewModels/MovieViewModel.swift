//
//  MovieViewModel.swift
//  MovieApp
//
//  Created by Martín González on 13/01/22.
//

import Foundation

public class MovieViewModel {
    
    var response = Observable(MovieResponse())
    var statusCode = Observable(CodeResponse.success)
    
    let defaults = ManagerUserDefaults()
    
    func getPopularMovies() {
        Api.requestMovies(url: ApiConstants.requestPopular) { [weak self] response, statusCode in
            guard let response = response else {
                self?.statusCode.value = statusCode
                return
            }
            
            self?.response.value = response
            self?.statusCode.value = statusCode
        }
        
    }
    
    
    func getTopRated() {
        Api.requestMovies(url: ApiConstants.requestTop) { [weak self] response, statusCode in
            guard let response = response else {
                self?.statusCode.value = statusCode
                return
            }
            
            self?.response.value = response
            self?.statusCode.value = statusCode
        }
    }
    
    func getOnTv() {
        Api.requestMovies(url: ApiConstants.requestOnTv) { [weak self] response, statusCode in
            guard let response = response else {
                self?.statusCode.value = statusCode
                return
            }
            
            self?.response.value = response
            self?.statusCode.value = statusCode
        }
    }
    
    func getAiring() {
        Api.requestMovies(url: ApiConstants.requestAiring) { [weak self] response, statusCode in
            guard let response = response else {
                self?.statusCode.value = statusCode
                return
            }
            
            self?.response.value = response
            self?.statusCode.value = statusCode
        }
    }
    
    
    func getFavoriteShows() {
        let url = ApiConstants.requestFavoriteShows(sessionId: defaults.getSessionId(key: "SESSION"))
        Api.requestMovies(url: url) { [weak self] response, statusCode in
            guard let response = response else {
                self?.statusCode.value = statusCode
                return
            }
            
            self?.response.value = response
            self?.statusCode.value = statusCode
        }
    }
    
}
