//
//  ProfileViewController.swift
//  MovieApp
//
//  Created by Martín González on 14/01/22.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileImage: CustomImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    
    let defaults = ManagerUserDefaults()
    
    var movieResponse = MovieResponse() {
        didSet {
            hideLoader()
            DispatchQueue.main.async { [self] in
                self.favoritesCollectionView.reloadData()
            }
        }
    }
    
    
    var viewModelMovies = MovieViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLayoutSubviews()
        
        // Do any additional setup after loading the view.
        favoritesCollectionView.register(MovieCollectionViewCell.nib(), forCellWithReuseIdentifier: MovieCollectionViewCell.identifier)
        favoritesCollectionView.dataSource = self
        favoritesCollectionView.delegate = self
        favoritesCollectionView.collectionViewLayout = UICollectionViewFlowLayout()
        
        favoritesCollectionView.contentInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        setupView()
        
        
        viewModelMovies.statusCode.bind { [weak self] code in
            self?.hideLoader()
            if code != .success {
                //handle
            }
            if code == .success {
                //handle
            }
        }
        
        viewModelMovies.response.bind { [weak self] movies in
            self?.movieResponse = movies
        }
        showLoader()
        viewModelMovies.getFavoriteShows()
    }
    
    
    func setupView() {
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        
        usernameLabel.text = "@" + defaults.getUserName(key: "USERNAME")
    }

    
    
}


extension ProfileViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieResponse.getCountOfMovies()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.identifier, for: indexPath) as! MovieCollectionViewCell

        cell.movie = movieResponse.movies![indexPath.row]
        cell.layoutIfNeeded()
        return cell
    }
}


extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 340)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
