//
//  ManagerUserDefaults.swift
//  MovieApp
//
//  Created by Martín González on 13/01/22.
//

import Foundation

class ManagerUserDefaults: NSObject {
    
    func saveStatusLogged(status: Int){
        UserDefaults.standard.set(status, forKey: "ISLOGGEDIN")
        UserDefaults.standard.synchronize()
    }
    
    func statusLogged(key: String) -> Int{
        let returnStatus: Int = UserDefaults.standard.integer(forKey: key)
        return  returnStatus
    }
    
    
    func saveToken(token: String){
        UserDefaults.standard.set(token, forKey: "TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func getToken(key: String) -> String{
        let returnStatus: String = UserDefaults.standard.string(forKey: key)!
        return  returnStatus
    }
    
    func saveSessionId(session: String){
        UserDefaults.standard.set(session, forKey: "SESSION")
        UserDefaults.standard.synchronize()
    }
    
    func getSessionId(key: String) -> String{
        let returnStatus: String = UserDefaults.standard.string(forKey: key)!
        return  returnStatus
    }
    
    func saveUserName(username: String){
        UserDefaults.standard.set(username, forKey: "USERNAME")
        UserDefaults.standard.synchronize()
    }
    
    func getUserName(key: String) -> String{
        let returnStatus: String = UserDefaults.standard.string(forKey: key)!
        return  returnStatus
    }
}
