//
//  Utilities.swift
//  MovieApp
//
//  Created by Martín González on 13/01/22.
//

import UIKit

class Utilities {
    static func styleBackViewMenu(_ view: UIView) {
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
    }
    
    static func roundButton(_ button: UIButton) {
        button.layer.cornerRadius = 6
    }
}
