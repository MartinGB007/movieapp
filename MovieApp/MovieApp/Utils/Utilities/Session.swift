//
//  Session.swift
//  MovieApp
//
//  Created by Martín González on 13/01/22.
//

import Foundation
import UIKit

class Session {
    
    
    public static func closeSession(actualView: UIView) {
        let defaults = ManagerUserDefaults()
        
        defaults.saveStatusLogged(status: 0)
        
        
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "loginVC")
        actualView.window?.rootViewController = rootController
        actualView.window?.makeKeyAndVisible()
    }
    
}
