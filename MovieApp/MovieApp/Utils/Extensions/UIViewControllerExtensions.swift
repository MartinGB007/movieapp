//
//  UIViewControllerExtensions.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import Foundation
import UIKit

extension UIViewController {
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
    
    // MARK: - Keyboards
    
    func enableKeyBoardToggle(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardWillChangeLogic(isShow: true, keyboardHeight: keyboardSize.height)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardWillChangeLogic(isShow: false, keyboardHeight: 0)
    }
    
    @objc func keyboardWillChangeLogic(isShow:Bool, keyboardHeight: CGFloat) {
        UIView.animate(withDuration: 0.25) {
            self.view.frame.origin.y = -(isShow ? keyboardHeight : 0)
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Loaders
    
    func showLoader(){
        hideLoader()
        
        let loader = LoaderView()
        view.addSubview(loader)
        loader.anchor(centerY: view.centerYAnchor, centerX: view.centerXAnchor)
        loader.anchor(top: nil,
                         leading: nil,
                         bottom: nil,
                         trailing: nil,
                         size: CGSize(width: view.frame.width, height: view.frame.height))
        
        loader.startIndicator()
        
    }
    
    func hideLoader(){
        view.subviews.forEach { currentView in
            if let loader = currentView as? LoaderView{
                loader.stopIndicator()
                loader.removeFromSuperview()
            }
        }
    }
}
