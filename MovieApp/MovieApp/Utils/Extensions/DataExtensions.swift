//
//  DataExtensions.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import Foundation

extension Data {
    func decodeData<T: Decodable>() -> (T?, Error?) {
        do {
            let response = try JSONDecoder().decode(T.self, from: self)
            return (response, nil)
        } catch let jsonError {
            print("Failed to decode JSON: ", jsonError)
            return(nil, jsonError)
        }
        
    }
}
