//
//  Api.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import Foundation

class Api {
    static let session: URLSession = URLSession.shared
    static let timeout: TimeInterval = 30
    
    static func makeURLRequest(url: URL,method: Method,parameters: [String:Any]?) -> URLRequest{
        var urlRequest:URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        //Content-Type
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.timeoutInterval = timeout
        if let params = parameters{
            urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }
        return urlRequest
    }
    
    static func requestToken(url: String,
                                method: Method = .GET,
                                parameters: [String: Any]? = nil,
                                completion: @escaping (RequestTokenResponse?, CodeResponse) -> ()) {
        //Check connection
        if !NetworkMonitor.isConnectedToNetwork() {
            completion(nil, .not_conecction)
            return
        }
        
        //Convert String to URL
        guard let urlForRequest = URL(string: url) else {
            print("API: WRONG URL \(url)")
            completion(nil, CodeResponse.bad_url)
            return
        }
        
        let request = makeURLRequest(url: urlForRequest, method: method, parameters: parameters)
        
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                guard let data = data, error == nil, let httpUrlRespose = response as? HTTPURLResponse, let code = CodeResponse(rawValue:  httpUrlRespose.statusCode) else {
                    //completion(nil, .no_info_registered)
                    completion(nil, .timeout)
                    return
                }
                
                if code == .unauthorized {
                    completion(nil, code)
                    return
                }
                if code == .other_error {
                    completion(nil, code)
                    return
                }
                var requestTokenResponse: RequestTokenResponse?
                do {
                    requestTokenResponse = try JSONDecoder().decode(RequestTokenResponse.self, from: data)
                    completion(requestTokenResponse, code)
                } catch {
                    completion(nil, .bad_decodable)
                }
            }
        }.resume()
    }
    
    static func requestSession(url: String,
                                method: Method = .GET,
                                parameters: [String: Any]? = nil,
                                completion: @escaping (RequestSessionResponse?, CodeResponse) -> ()) {
        //Check connection
        if !NetworkMonitor.isConnectedToNetwork() {
            completion(nil, .not_conecction)
            return
        }
        
        //Convert String to URL
        guard let urlForRequest = URL(string: url) else {
            print("API: WRONG URL \(url)")
            completion(nil, CodeResponse.bad_url)
            return
        }
        
        let request = makeURLRequest(url: urlForRequest, method: method, parameters: parameters)
        
        
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                guard let data = data, error == nil, let httpUrlRespose = response as? HTTPURLResponse, let code = CodeResponse(rawValue:  httpUrlRespose.statusCode) else {
                    //completion(nil, .no_info_registered)
                    completion(nil, .timeout)
                    return
                }
                
                if code == .unauthorized {
                    completion(nil, code)
                    return
                }
                if code == .other_error {
                    completion(nil, code)
                    return
                }
                var requestSessionResponse: RequestSessionResponse?
                do {
                    requestSessionResponse = try JSONDecoder().decode(RequestSessionResponse.self, from: data)
                    completion(requestSessionResponse, code)
                } catch {
                    print(error)
                    completion(nil, .bad_decodable)
                }
            }
        }.resume()
    }
    
    static func requestMakeLogin(url: String,
                                method: Method = .GET,
                                parameters: [String: Any]? = nil,
                                completion: @escaping (RequestTokenResponse?, CodeResponse) -> ()) {
        //Check connection
        if !NetworkMonitor.isConnectedToNetwork() {
            completion(nil, .not_conecction)
            return
        }
        
        //Convert String to URL
        guard let urlForRequest = URL(string: url) else {
            print("API: WRONG URL \(url)")
            completion(nil, CodeResponse.bad_url)
            return
        }
        
        let request = makeURLRequest(url: urlForRequest, method: method, parameters: parameters)
        
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                guard let data = data, error == nil, let httpUrlRespose = response as? HTTPURLResponse, let code = CodeResponse(rawValue:  httpUrlRespose.statusCode) else {
                    //completion(nil, .no_info_registered)
                    completion(nil, .timeout)
                    return
                }
                
                var requestTokenResponse: RequestTokenResponse?
                do {
                    requestTokenResponse = try JSONDecoder().decode(RequestTokenResponse.self, from: data)
                    completion(requestTokenResponse, code)
                } catch {
                    print(error)
                    completion(nil, .bad_decodable)
                }
            }
        }.resume()
    }
    
    static func requestMovies(url: String,
                                method: Method = .GET,
                                parameters: [String: Any]? = nil,
                                completion: @escaping (MovieResponse?, CodeResponse) -> ()) {
        //Check connection
        if !NetworkMonitor.isConnectedToNetwork() {
            completion(nil, .not_conecction)
            return
        }
        
        //Convert String to URL
        guard let urlForRequest = URL(string: url) else {
            print("API: WRONG URL \(url)")
            completion(nil, CodeResponse.bad_url)
            return
        }
        
        let request = makeURLRequest(url: urlForRequest, method: method, parameters: parameters)
        
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                guard let data = data, error == nil, let httpUrlRespose = response as? HTTPURLResponse, let code = CodeResponse(rawValue:  httpUrlRespose.statusCode) else {
                    //completion(nil, .no_info_registered)
                    completion(nil, .timeout)
                    return
                }
                
                var movieResponse: MovieResponse?
                do {
                    movieResponse = try JSONDecoder().decode(MovieResponse.self, from: data)
                    completion(movieResponse, code)
                } catch {
                    print(error)
                    completion(nil, .bad_decodable)
                }
            }
        }.resume()
    }
    
    
    enum Method: String {
        case GET
        case POST
    }
}

enum CodeResponse: Int {
    case success = 200
    case no_info_registered = 201
    case bad_request = 400
    case unauthorized = 401
    case other_error = 402
    case forbiden = 403
    case suspendido = 405
    case eliminado = 406
    case precodition_failed = 412
    case error_server = 500
    case not_conecction = -1001
    case timeout = -1002
    case bad_url = -1003
    case bad_decodable = -1004
    case copy_file_error = -1005
}
