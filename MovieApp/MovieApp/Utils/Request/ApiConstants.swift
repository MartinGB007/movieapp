//
//  ApiConstants.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import Foundation

class ApiConstants {
    //Ambiente QA
    static let baseURL = "https://api.themoviedb.org/3"
    static let baseImagesURL = "https://image.tmdb.org/t/p/w300"
    static let baseAuth = "\(baseURL)/authentication"
    static let apiKey = "58de2b1aa8485c51a4e030104d43f139"
    
    
    //Request token
    static let requestToken = "\(baseAuth)/token/new?api_key=\(apiKey)"
    
    //Request token
    static let requestSession = "\(baseAuth)/session/new?api_key=\(apiKey)"
    
    //Request login
    static let requestLogin = "\(baseAuth)/token/validate_with_login?api_key=\(apiKey)"
    
    //Request popular
    static let requestPopular = "\(baseURL)/movie/popular?api_key=\(apiKey)"
    
    //Request top
    static let requestTop = "\(baseURL)/movie/top_rated?api_key=\(apiKey)"
    
    //Request on tv
    static let requestOnTv = "\(baseURL)/tv/on_the_air?api_key=\(apiKey)"
    
    //Request airing
    static let requestAiring = "\(baseURL)/tv/airing_today?api_key=\(apiKey)"
    
    //Request favorites
    static func requestFavoriteShows(sessionId: String) -> String {
        let favoritesShowsURL = "\(baseURL)/account/11168022/favorite/tv?session_id=\(sessionId)&api_key=\(apiKey)"
    
        return favoritesShowsURL
    }
    
    //Movie image
    static func getMovieImage(fileName: String) -> String{
        let stringUrl = "\(baseImagesURL)\(fileName)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return stringUrl!
    }
}
