//
//  CustomImageView.swift
//  MovieApp
//
//  Created by Martín González on 13/01/22.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    
    var imageURLString: String?
    
    
    
    func downloaded(from url: URL?, contentMode mode: ContentMode = .scaleAspectFill,retries: Int = 3) {
        
        contentMode = .scaleToFill
        guard let url = url else {
            return
        }
        imageURLString = url.absoluteString
        if let cachedImage = imageCache.object(forKey: url as AnyObject) as? UIImage{
            self.contentMode = mode
            loadFade(cachedImage)
        }
        
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 10.0
        sessionConfig.timeoutIntervalForResource = 10.0
        let session = URLSession(configuration: sessionConfig)
        session.dataTask(with: url) { [weak self] data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                if retries > 0{
                    DispatchQueue.main.async {
                        print("URL request for image \(url)")
                        self?.downloaded(from: url,retries: retries - 1)
                    }
                }
                print("Fallo la carga de la imagen retries \(retries)")
                return
                
            }
            DispatchQueue.main.async() {
                if self?.imageURLString == url.absoluteString{
                    self?.contentMode = mode
                    self?.loadFade(image)
                }
                imageCache.setObject(image, forKey: url as AnyObject)
                self?.image = image
            }
        }.resume()
        
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
    
    func loadFade(_ image: UIImage){
        UIView.transition(with: self, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.image = image
            
        }, completion: nil)
        
    }
}

