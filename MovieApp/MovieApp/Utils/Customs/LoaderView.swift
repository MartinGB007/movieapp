//
//  LoaderView.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import UIKit

class LoaderView: UIView{
    
    private lazy var actInd:UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView()
        aiv.style = UIActivityIndicatorView.Style.large
        //aiv.style = .whiteLarge
        return aiv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit(){
       
        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
       
        loadingView.addSubview(actInd)
        
        actInd.anchor(centerY: loadingView.centerYAnchor, centerX: loadingView.centerXAnchor)
        actInd.anchor(top: nil,
                         leading: nil,
                         bottom: nil,
                         trailing: nil,
                         size: CGSize(width: 80, height: 80))
       addSubview(loadingView)
        
        loadingView.anchor(centerY: centerYAnchor, centerX: centerXAnchor)
        loadingView.anchor(top: nil,
                         leading: nil,
                         bottom: nil,
                         trailing: nil,
                         size: CGSize(width: 80, height: 80))
    }
    
    
    func startIndicator(){
        actInd.startAnimating()
    }
    
    func stopIndicator() {
        actInd.startAnimating()
    }
}

