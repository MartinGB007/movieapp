//
//  Observable.swift
//  MovieApp
//
//  Created by Martín González on 12/01/22.
//

import Foundation

//Observable
class Observable<T> {
    private var listener: [((T) -> Void)] = []
    
    var value: T {
        didSet {
            listener.forEach{$0(value)}
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(_ listener: @escaping (T) -> Void) {
        self.listener.append(listener)
    }
}
